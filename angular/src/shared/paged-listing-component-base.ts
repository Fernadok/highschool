import { AppComponentBase } from "shared/app-component-base";
import { Component, Injector, OnInit } from "@angular/core";
import { IPagedList } from "./service-proxies/service-proxies";
import { BsModalService, BsModalRef, ModalOptions } from "ngx-bootstrap/modal";

export class PagedResultDto {
  items: any[];
  totalCount: number;
}

export class EntityDto {
  id: number;
}

export class PagedRequestDto {
  skipCount: number;
  maxResultCount: number;
}

export interface ContextDto<TEntityDto> {
  data: TEntityDto[] | undefined;
  metaData: IPagedList;
}

@Component({
  template: "",
})
export abstract class PagedListingComponentBase<TEntityDto>
  extends AppComponentBase
  implements OnInit
{
  public pageSize = 10;
  public pageNumber = 1;
  public totalPages = 1;
  public totalItems: number;
  public isTableLoading = false;

  private _modalService: BsModalService;

  constructor(injector: Injector, modalService: BsModalService) {
    super(injector);
    this._modalService = modalService;
  }

  ngOnInit(): void {
    this.refresh();
  }

  refresh(): void {
    this.getDataPage(this.pageNumber);
  }

  public showPaging(result: PagedResultDto, pageNumber: number): void {
    this.totalPages =
      (result.totalCount - (result.totalCount % this.pageSize)) /
        this.pageSize +
      1;

    this.totalItems = result.totalCount;
    this.pageNumber = pageNumber;
  }

  public showPagingContextDto(
    result: ContextDto<TEntityDto>,
    pageNumber: number
  ): void {
    this.totalPages =
      (result.metaData.totalItemCount -
        (result.metaData.totalItemCount % this.pageSize)) /
        this.pageSize +
      1;

    this.totalItems = result.metaData.totalItemCount;
    this.pageNumber = pageNumber;
  }

  public getDataPage(page: number): void {
    const req = new PagedRequestDto();
    req.maxResultCount = this.pageSize;
    req.skipCount = (page - 1) * this.pageSize;

    this.isTableLoading = true;
    this.list(req, page, () => {
      this.isTableLoading = false;
    });
  }

  protected abstract list(
    request: PagedRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void;

  protected abstract delete(entity: TEntityDto): void;

  public showCreateOrEditDialog(content: any, config?: ModalOptions): void {
    let createOrEditDialog: BsModalRef;
    createOrEditDialog = this._modalService.show(content, config);

    createOrEditDialog.content.onSave.subscribe(() => {
      this.refresh();
    });
  }
}

import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output,
} from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import {
  LocacionDto,
  LocacionServiceServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { BsModalRef } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";

@Component({
  templateUrl: "./locacion-modal.component.html",
})
export class LocacionModalComponent extends AppComponentBase implements OnInit {
  id: number;
  title = "Crear";
  saving = false;
  locacion = new LocacionDto();

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _locacionService: LocacionServiceServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.title = !this.id ? "Nuevo Barrio" : "Editar locación";

    this._locacionService.getById(this.id).subscribe((result: LocacionDto) => {
      this.locacion = result;
    });
  }

  save(): void {
    this.saving = true;
    this._locacionService
      .addOrUpdate(this.locacion)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}

import { Component, Injector } from "@angular/core";
import { finalize } from "rxjs/operators";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
  ContextDto,
  PagedListingComponentBase,
  PagedRequestDto,
} from "shared/paged-listing-component-base";
import {
  LocacionServiceServiceProxy,
  LocacionDto,
  LocacionDtoContextDto,
} from "@shared/service-proxies/service-proxies";
import { LocacionModalComponent } from "./locacion-modal/locacion-modal.component";

class PagedLocacionsRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  selector: "app-locaciones",
  templateUrl: "./locaciones.component.html",
  animations: [appModuleAnimation()],
})
export class LocacionesComponent extends PagedListingComponentBase<LocacionDto> {
  locaciones: LocacionDto[] = [];
  keyword = "";
  isActive: boolean | null;

  filter = {
    Id: 0,
    Search: "",
    Page: 1,
    PageSize: 10,
    OrderColumn: "Denominacion",
    Order: "asc",
  };

  constructor(
    injector: Injector,
    private _locacionService: LocacionServiceServiceProxy,
    _modalService: BsModalService
  ) {
    super(injector, _modalService);
  }

  clearFilters(): void {
    this.keyword = "";
    this.isActive = undefined;
    this.getDataPage(1);
  }

  create(): void {
    this.showCreateOrEditUserDialog();
  }

  edit(locacion: LocacionDto): void {
    this.showCreateOrEditUserDialog(locacion.id);
  }
  private showCreateOrEditUserDialog(id?: number): void {
    let configs = {
      class: "modal-lg",
      initialState: {
        id: id,
      },
    };

    this.showCreateOrEditDialog(LocacionModalComponent, configs);
  }

  protected list(
    request: PagedLocacionsRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._locacionService
      .get(
        this.filter.Id,
        this.keyword,
        pageNumber,
        this.filter.Order,
        this.filter.PageSize,
        this.filter.OrderColumn,
        null,
        null
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: ContextDto<LocacionDto>) => {
        this.locaciones = result.data;
        this.showPagingContextDto(result, pageNumber);
      });
  }

  protected delete(locacion: LocacionDto): void {
    abp.message.confirm(
      this.l("Quiere borrar: " + locacion.denominacion),
      undefined,
      (result: boolean) => {
        if (result) {
          this._locacionService.delete(locacion.id).subscribe(() => {
            abp.notify.success(this.l("SuccessfullyDeleted"));
            this.refresh();
          });
        }
      }
    );
  }
}

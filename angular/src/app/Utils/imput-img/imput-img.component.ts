import { Component, EventEmitter, Input, OnInit, Output } from "@angular/core";
import { toBase64 } from "@shared/filesUtils/fileUtilities";
import { FileDto } from "@shared/service-proxies/service-proxies";

@Component({
  selector: "app-imput-img",
  templateUrl: "./imput-img.component.html",
  styleUrls: ["./imput-img.component.css"],
})
export class ImputImgComponent implements OnInit {
  file: File;
  imageBase64: string;

  @Output() archivoSeleccionadoEvent = new EventEmitter<File>();

  @Input() archviActual: FileDto;

  constructor() {}

  ngOnInit(): void {}

  change(event) {
    if (event.target.files.length > 0) {
      this.file = event.target.files[0];
      toBase64(this.file)
        .then((value: string) => {
          this.imageBase64 = value;
          this.archivoSeleccionadoEvent.emit(this.file);
        })
        .catch((error) => console.error(error));
    }
  }
  delete() {
    this.archviActual = null;
  }
}

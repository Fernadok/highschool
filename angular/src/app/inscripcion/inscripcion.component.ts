import { Component, Injector, OnInit } from "@angular/core";
import { finalize } from "rxjs/operators";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
  ContextDto,
  PagedListingComponentBase,
  PagedRequestDto,
} from "shared/paged-listing-component-base";
import { 
  ComisioneServiceServiceProxy,
  ComisioneDto,
  ComisioneDtoContextDto,
} from "@shared/service-proxies/service-proxies";
import { ComisioneModalComponent } from "@app/comisiones/comisione-modal/comisione-modal.component";
import { InscripcionModalComponent } from "./inscripcion-modal/inscripcion-modal.component";

class PagedLocacionsRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

class PagedIntegrantesRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  selector: 'app-inscripcion',
  templateUrl: './inscripcion.component.html',
  animations: [appModuleAnimation()],
  styleUrls: ['./inscripcion.component.css']
})
export class InscripcionComponent
  extends PagedListingComponentBase<ComisioneDto> 
  implements OnInit {

    Comisiones: ComisioneDto[] = [];
    keyword = "";
    isActive: boolean | null;
  
    filter = {
      Id: 0,
      Search: "",
      Page: 1,
      PageSize: 10,
      OrderColumn: "FechaInicio",
      Order: "asc",
    };
  
    constructor(
      injector: Injector,
      private _comisioneService: ComisioneServiceServiceProxy,
      _modalService: BsModalService
    ) {
      super(injector, _modalService);
    }
    
    clearFilters(): void {
      this.keyword = "";
      this.isActive = undefined;
      this.getDataPage(1);
    }
  
    create(): void {
      this.showCreateOrEditInscriptionDialog();
    }
  
    inscribir(comisione: ComisioneDto): void {
      this.showCreateOrEditInscriptionDialog(comisione);
    }
    private showCreateOrEditInscriptionDialog(comisione?: ComisioneDto): void {
      let configs = {
        class: "modal-lg",
        initialState: {
          id: comisione?.id,
          // cupos: comisione.cupo,
          // cuposVacantes: comisione.cuposVacantes
        },
      };
  
      this.showCreateOrEditDialog(InscripcionModalComponent, configs);
    }
  
    protected list(
      request: PagedLocacionsRequestDto,
      pageNumber: number,
      finishedCallback: Function
    ): void {
      request.keyword = this.keyword;
      request.isActive = this.isActive;
  
      this._comisioneService
        .get(
          this.filter.Id,
          this.keyword,
          pageNumber,
          this.filter.Order,
          this.filter.PageSize,
          this.filter.OrderColumn,
          null,
          null
        )
        .pipe(
          finalize(() => {
            finishedCallback();
          })
        )
        .subscribe((result: ContextDto<ComisioneDto>) => {
          this.Comisiones = result.data;
          this.showPagingContextDto(result, pageNumber);
        });
    }
  
    protected delete(cursp: ComisioneDto): void {
      abp.message.confirm(
        this.l("Quiere borrar: " + cursp.nombre),
        undefined,
        (result: boolean) => {
          if (result) {
            this._comisioneService.delete(cursp.id).subscribe(() => {
              abp.notify.success(this.l("SuccessfullyDeleted"));
              this.refresh();
            });
          }
        }
      );
    }
  }
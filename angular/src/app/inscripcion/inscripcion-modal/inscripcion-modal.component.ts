import { FormControl } from "@angular/forms";
import { Observable } from "rxjs";
import { map, startWith } from "rxjs/operators";

import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output,
} from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import {
  ContextDto,
  PagedListingComponentBase,
  PagedRequestDto,
} from "@shared/paged-listing-component-base";
import {
  IntegranteDto,
  IntegrantesComisioneDto,
  IntegranteServiceServiceProxy,
  ComisioneIntegranteDto,
  ComisioneIntegranteDtoContextDto,
  ComisioneIntegranteServiceServiceProxy,
  Int64IdDescription,
  LocacionDto,
  LocacionServiceServiceProxy,
} from "@shared/service-proxies/service-proxies";
import { BsModalRef, BsModalService } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";
import { threadId } from "worker_threads";
import { IntegranteModalComponentComponent } from "@app/integrantes/integrante-modal-component/integrante-modal-component.component";

class PagedIntegrantesRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  selector: "app-inscripcion-modal",
  templateUrl: "./inscripcion-modal.component.html",
  styleUrls: ["./inscripcion-modal.component.css"],
})
export class InscripcionModalComponent
  extends PagedListingComponentBase<IntegranteDto>
  implements OnInit
{
  myControl = new FormControl();
  options: string[] = ["One", "Two", "Three"];
  filteredOptions: Observable<string[]>;

  comisioneIntegranteDto: ComisioneIntegranteDto = new ComisioneIntegranteDto();
  alumnos: IntegranteDto[] = [];
  keyword = "";
  isActive: boolean | null;
  filter = {
    Id: 0,
    Search: "",
    Page: 1,
    PageSize: 10,
    OrderColumn: "Id",
    Order: "desc",
  };

  id: number;
  cupos: number;
  cuposVacantes: number;
  title = "Crear";
  saving = false;
  locacion = new LocacionDto();
  keyword2 = "description";
  data: Int64IdDescription[];
  integranteComisione = new IntegrantesComisioneDto();
  alumnosComisione: IntegrantesComisioneDto[] = [];
  alumnosComisionePaginate: IntegrantesComisioneDto[] = [];
  hayCupo : boolean = false;
  autoridad = [
    {
      id: "Presidente",
      tipo: "Presidente",
    },
    {
      id: "Vicepresidente",
      tipo: "Vicepresidente",
    },
    {
      id: "Otro",
      tipo: "Otro",
    },
  ];
  opcionSeleccionado: string = "0";

  filterIntegrante=
  {
    search:''
  };

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    private _alumnoService: IntegranteServiceServiceProxy,
    private _comisioneIntegranteService: ComisioneIntegranteServiceServiceProxy,
    public _locacionService: LocacionServiceServiceProxy,
    public bsModalRef: BsModalRef,
    _modalService: BsModalService
  ) {
    super(injector, _modalService);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.options.filter((option) =>
      option.toLowerCase().includes(filterValue)
    );
  }

  clearFilters(): void {
    this.keyword = "";
    this.isActive = undefined;
    this.getDataPage(1);
  }
  capturar() {
      this.integranteComisione.tipoAutoridad= this.opcionSeleccionado;
  }

  ngOnInit(): void {
    this.title = !this.id ? "Nueva locación" : "Inscribir Integrante" ;
    this.getDataPage(1);

    // this._alumnoService
    //   .integranteList()
    //   .subscribe((result: Int64IdDescription[]) => {
    //     this.data = result;
    //   });

  }

  ngAfterViewInit(){
  
  }

  protected list(
    request: PagedIntegrantesRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._comisioneIntegranteService
      .getById(this.id)
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: ComisioneIntegranteDto) => {

        result.comisioneIntegrantes.forEach((element) => {
          let alumnosComisioneDto = new IntegrantesComisioneDto();
          alumnosComisioneDto.id = element.id;
          alumnosComisioneDto.comisioneId = this.id;
          alumnosComisioneDto.integranteId = element.integranteId;
          alumnosComisioneDto.calificacion = element.calificacion;
          alumnosComisioneDto.tipoAutoridad=element.tipoAutoridad;
          alumnosComisioneDto.observacion = element.observacion;
          alumnosComisioneDto.nombreCompleto = element.nombreCompleto;
          this.alumnosComisione.push(alumnosComisioneDto);
        });
          
        this.alumnosComisionePaginate = this.alumnosComisione;
        this.hayCupo = this.cuposVacantes > 0;
      });
  }

  protected delete(alumno: IntegranteDto): void {
    abp.message.confirm(
      this.l("Quiere borrar: " + alumno.apellido),
      undefined,
      (result: boolean) => {
        if (result) {
          this._alumnoService.delete(alumno.id).subscribe(() => {
            abp.notify.success(this.l("SuccessfullyDeleted"));
            this.refresh();
          });
        }
      }
    );
  }

  save(): void {
    this.saving = true;

    this.comisioneIntegranteDto.id = this.id;
    this.comisioneIntegranteDto.comisioneIntegrantes = [];
    this.alumnosComisione.forEach((element) => {
      let alumnosComisioneDto = new IntegrantesComisioneDto();
      alumnosComisioneDto.id = element.id;
      alumnosComisioneDto.comisioneId = element.comisioneId;
      alumnosComisioneDto.integranteId = element.integranteId;
      alumnosComisioneDto.calificacion = element.calificacion;
      alumnosComisioneDto.tipoAutoridad=element.tipoAutoridad;
      alumnosComisioneDto.observacion = element.observacion;
      

      this.comisioneIntegranteDto.comisioneIntegrantes.push(alumnosComisioneDto);
    });

    this._comisioneIntegranteService
      .addOrUpdate(this.comisioneIntegranteDto)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }

  selectEvent(item: Int64IdDescription) {
    if (this.alumnosComisione.find((elem) => elem.integranteId === item.id)) {
      this.notify.warn("La persona ya se encuentra registrada en la comision.");
      return;
    }
   
    if (this.alumnosComisione.length == this.cupos)   {
      this.notify.warn("No hay cupos disponibles.");
      return;
    }

    const alumno = new IntegrantesComisioneDto();
    alumno.comisioneId = this.id;
    alumno.integranteId = item.id;
    alumno.nombreCompleto = item.description;
    this.alumnosComisione.push(alumno);
     
    this.alumnosComisionePaginate =this.alumnosComisione;
  }

  onChangeSearch(val: string) {
    this._alumnoService.integranteList2(val)
    .pipe(
      finalize(() => {
        this.saving = false;
      })
    ).subscribe((result:Int64IdDescription[]) => {
      this.data = result;
    });
  }

  onFocused(e) {
    // do something when input is focused
  }

  deleteIntegranteList(item: IntegrantesComisioneDto) {
      this._comisioneIntegranteService.delete(item.id).subscribe(() => {
      this.alumnosComisione = this.alumnosComisione.filter((elem) => elem.integranteId != item.integranteId)
      this.alumnosComisionePaginate=this.alumnosComisione.filter((elem) => elem.integranteId != item.integranteId)

      this.hayCupo = !(this.alumnosComisione.length == this.cupos);
      if (this.alumnosComisione.length == this.cupos)    {
        this.notify.warn("No hay cupos disponibles.");
        return;
      }
    });
  }

  nuevoIntegrante()
  {
    let configs = {
      class: "modal-lg",
      backdrop: true,
      ignoreBackdropClick: true
    };

    this.showCreateOrEditDialog(IntegranteModalComponentComponent, configs);
 
  }

  getDataIntegrantesFielter(query:string): void{
    console.log(event);


   let result = this.alumnosComisione.filter(x => x.nombreCompleto.toLocaleLowerCase().indexOf(query.toLocaleLowerCase()) > -1);
   this.alumnosComisionePaginate = result;
  }

  getDataPageIntegrantes(event): void{
    console.log(event);
    
    let result =  this.paginator(this.alumnosComisione,event,this.filter.PageSize);
    this.alumnosComisionePaginate = result.data;
    this.pageSize = this.filter.PageSize;
    this.pageNumber = event;
    this.totalItems = result.total;

  }

  paginator(items:any, page:number, per_page:number) {
 
    var page = page || 1,
    per_page = per_page || 10,
    offset = (page - 1) * per_page,
   
    paginatedItems = items.slice(offset).slice(0, per_page),
    total_pages = Math.ceil(items.length / per_page);
    return {
    page: page,
    per_page: per_page,
    pre_page: page - 1 ? page - 1 : null,
    next_page: (total_pages > page) ? page + 1 : null,
    total: items.length,
    total_pages: total_pages,
    data: paginatedItems
    };
  }
}

import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientJsonpModule } from "@angular/common/http";
import { HttpClientModule } from "@angular/common/http";
import { ModalModule } from "ngx-bootstrap/modal";
import { BsDropdownModule } from "ngx-bootstrap/dropdown";
import { CollapseModule } from "ngx-bootstrap/collapse";
import { TabsModule } from "ngx-bootstrap/tabs";
import { NgxPaginationModule } from "ngx-pagination";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { ServiceProxyModule } from "@shared/service-proxies/service-proxy.module";
import { SharedModule } from "@shared/shared.module";
import { HomeComponent } from "@app/home/home.component";
import { AboutComponent } from "@app/about/about.component";
// roles
import { RolesComponent } from "@app/roles/roles.component";
import { CreateRoleDialogComponent } from "./roles/create-role/create-role-dialog.component";
import { EditRoleDialogComponent } from "./roles/edit-role/edit-role-dialog.component";
// users
import { UsersComponent } from "@app/users/users.component";
import { CreateUserDialogComponent } from "@app/users/create-user/create-user-dialog.component";
import { EditUserDialogComponent } from "@app/users/edit-user/edit-user-dialog.component";
import { ChangePasswordComponent } from "./users/change-password/change-password.component";
import { ResetPasswordDialogComponent } from "./users/reset-password/reset-password.component";
// layout
import { HeaderComponent } from "./layout/header.component";
import { HeaderLeftNavbarComponent } from "./layout/header-left-navbar.component";
import { HeaderLanguageMenuComponent } from "./layout/header-language-menu.component";
import { HeaderUserMenuComponent } from "./layout/header-user-menu.component";
import { FooterComponent } from "./layout/footer.component";
import { SidebarComponent } from "./layout/sidebar.component";
import { SidebarLogoComponent } from "./layout/sidebar-logo.component";
import { SidebarUserPanelComponent } from "./layout/sidebar-user-panel.component";
import { SidebarMenuComponent } from "./layout/sidebar-menu.component";
import { LocacionesComponent } from "./locaciones/locaciones.component";
import { IntegrantesComponent } from "./integrantes/integrantes.component";
import { ComisionesComponent } from "./comisiones/comisiones.component";
import { LocacionModalComponent } from "./locaciones/locacion-modal/locacion-modal.component";
import { IntegranteModalComponentComponent } from "./integrantes/integrante-modal-component/integrante-modal-component.component";
import { ComisioneModalComponent } from "./comisiones/comisione-modal/comisione-modal.component";
import { DemoComponent } from "./demo/demo.component";
import { ImputImgComponent } from "./Utils/imput-img/imput-img.component";
import { InscripcionComponent } from "./inscripcion/inscripcion.component";
import { InscripcionModalComponent } from "./inscripcion/inscripcion-modal/inscripcion-modal.component";
import { MateriaModule } from "@shared/material.module";
import { AutocompleteLibModule } from "angular-ng-autocomplete";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    // roles
    RolesComponent,
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    UsersComponent,
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ChangePasswordComponent,
    ResetPasswordDialogComponent,
    // layout
    HeaderComponent,
    HeaderLeftNavbarComponent,
    HeaderLanguageMenuComponent,
    HeaderUserMenuComponent,
    FooterComponent,
    SidebarComponent,
    SidebarLogoComponent,
    SidebarUserPanelComponent,
    SidebarMenuComponent,
    LocacionesComponent,
    IntegrantesComponent,
    ComisionesComponent,
    LocacionModalComponent,
    IntegranteModalComponentComponent,
    ComisioneModalComponent,
    DemoComponent,
    ImputImgComponent,
    InscripcionComponent,
    InscripcionModalComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule,
    ModalModule.forChild(),
    BsDropdownModule,
    CollapseModule,
    TabsModule,
    AppRoutingModule,
    ServiceProxyModule,
    SharedModule,
    MateriaModule,
    NgxPaginationModule,
    AutocompleteLibModule,
  ],
  providers: [],
  entryComponents: [
    // roles
    CreateRoleDialogComponent,
    EditRoleDialogComponent,
    // users
    CreateUserDialogComponent,
    EditUserDialogComponent,
    ResetPasswordDialogComponent,
    LocacionModalComponent,
    IntegranteModalComponentComponent,
    ComisioneModalComponent,
    InscripcionModalComponent,
  ],
})
export class AppModule {}

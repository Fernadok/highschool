import { Component, Injector, OnInit } from "@angular/core";
import { finalize } from "rxjs/operators";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
  ContextDto,
  PagedListingComponentBase,
  PagedRequestDto,
} from "shared/paged-listing-component-base";
import {
  ComisioneServiceServiceProxy,
  ComisioneDto,
  ComisioneDtoContextDto,
} from "@shared/service-proxies/service-proxies";
import { ComisioneModalComponent } from "@app/comisiones/comisione-modal/comisione-modal.component";

import {InscripcionModalComponent} from "@app/inscripcion/inscripcion-modal/inscripcion-modal.component";

class PagedLocacionsRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

class PagedIntegrantesRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  selector: "app-comisiones",
  templateUrl: "./comisiones.component.html",
  animations: [appModuleAnimation()],
})
export class ComisionesComponent
  extends PagedListingComponentBase<ComisioneDto>
  implements OnInit
{
  Comisiones: ComisioneDto[] = [];
  keyword = "";
  isActive: boolean | null;

  filter = {
    Id: 0,
    Search: "",
    Page: 1,
    PageSize: 10,
    OrderColumn: "Nombre",
    Order: "asc",
  };

  constructor(
    injector: Injector,
    private _comisioneService: ComisioneServiceServiceProxy,
    _modalService: BsModalService
  ) {
    super(injector, _modalService);
  }

  clearFilters(): void {
    this.keyword = "";
    this.isActive = undefined;
    this.getDataPage(1);
  }

  create(): void {
    this.showCreateOrEditUserDialog();
  }

  edit(comisione: ComisioneDto): void {
    this.showCreateOrEditUserDialog(comisione.id);
  }
  private showCreateOrEditUserDialog(id?: number): void {
    let configs = {
      class: "modal-lg",
      initialState: {
        id: id,
      },
    };

    this.showCreateOrEditDialog(ComisioneModalComponent, configs);
  }

  protected list(
    request: PagedLocacionsRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._comisioneService
      .get(
        this.filter.Id,
        this.keyword,
        pageNumber,
        this.filter.Order,
        this.filter.PageSize,
        this.filter.OrderColumn,
        null,
        null
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: ContextDto<ComisioneDto>) => {
        this.Comisiones = result.data;
        this.showPagingContextDto(result, pageNumber);
      });
  }

  protected delete(cursp: ComisioneDto): void {
    abp.message.confirm(
      this.l("Quiere borrar: " + cursp.nombre),
      undefined,
      (result: boolean) => {
        if (result) {
          this._comisioneService.delete(cursp.id).subscribe(() => {
            abp.notify.success(this.l("SuccessfullyDeleted"));
            this.refresh();
          });
        }
      }
    );
  }
  inscribir(id)
  {
    let configs = {
      class: "modal-lg",
      backdrop: true,
      ignoreBackdropClick: true,
      initialState: {
        id: id
      },
    };

    this.showCreateOrEditDialog(InscripcionModalComponent, configs);
 
  }
}
// function InscripcionModalComponent(
//   InscripcionModalComponent: any,
//   configs: { class: string; initialState: { id: number } }
// ) {
//   throw new Error("Function not implemented.");
// }


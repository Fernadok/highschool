import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output,
} from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import {
  ComisioneDto,
  ComisioneDtoContextDto,
  ComisioneServiceServiceProxy,
  Int64IdDescription,
  LocacionServiceServiceProxy,
} from "@shared/service-proxies/service-proxies";
import * as moment from "moment";
import { BsModalRef } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";
import { threadId } from "worker_threads";

@Component({
  selector: "app-comisione-modal",
  templateUrl: "./comisione-modal.component.html",
})
export class ComisioneModalComponent extends AppComponentBase implements OnInit {
  id: number;
  title = "Crear";
  saving = false;
  Comisione = new ComisioneDto();
  capSelect: number = 0;
  locSelect: number = 0;
  CapacitadorList: Int64IdDescription[];
  LocacionList: Int64IdDescription[];

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _Comisioneervice: ComisioneServiceServiceProxy,
    public _locacionService: LocacionServiceServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);
  }

  ngOnInit(): void {
    this.title = !this.id ? "Nuevo Comisione" : "Editar Comisione";

    this._locacionService
      .locacionesList()
      .subscribe((result: Int64IdDescription[]) => {
        this.LocacionList = result;
      });
      // --
      if (this.id) {
        this._Comisioneervice.getById(this.id).subscribe((result: ComisioneDto) => {
          this.Comisione = result;
          this.Comisione.fechaInicio = moment(this.Comisione.fechaInicio);
          this.locSelect = this.Comisione.locacionId;
        });
      }
  }

  get(): void {
    this._Comisioneervice.getById(this.id).subscribe((result: ComisioneDto) => {
      this.Comisione = result;

      this.Comisione.fechaInicio = moment(this.Comisione.fechaInicio);

      this.locSelect = this.Comisione.locacionId;
    });
  }

  capturar(target: string): void {
    if (target == "LOC") this.Comisione.locacionId = this.locSelect;
  }

  save(): void {
    this.Comisione.fechaInicio = moment(this.Comisione.fechaInicio);

    this.saving = true;
    this._Comisioneervice
      .addOrUpdate(this.Comisione)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }
}

import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { AppRouteGuard } from "@shared/auth/auth-route-guard";
import { HomeComponent } from "./home/home.component";
import { AboutComponent } from "./about/about.component";
import { UsersComponent } from "./users/users.component";
import { RolesComponent } from "app/roles/roles.component";
import { ChangePasswordComponent } from "./users/change-password/change-password.component";
import { LocacionesComponent } from "./locaciones/locaciones.component";
import { IntegrantesComponent } from "./integrantes/integrantes.component";
import { ComisionesComponent } from "./comisiones/comisiones.component";
import { InscripcionComponent } from "./inscripcion/inscripcion.component";

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: "",
        component: AppComponent,
        children: [
          {
            path: "home",
            component: HomeComponent,
            canActivate: [AppRouteGuard],
          },
          {
            path: "comisiones",
            component: ComisionesComponent,
            data: { permission: "Pages.Comisiones" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "integrantes",
            component: IntegrantesComponent,
            data: { permission: "Pages.Integrantes" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "locaciones",
            component: LocacionesComponent,
            data: { permission: "Pages.Locacioines" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "inscripcion",
            component: InscripcionComponent,
            data: { permission: "Pages_Inscripcion" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "users",
            component: UsersComponent,
            data: { permission: "Pages.Users" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "roles",
            component: RolesComponent,
            data: { permission: "Pages.Roles" },
            canActivate: [AppRouteGuard],
          },
          {
            path: "update-password",
            component: ChangePasswordComponent,
            canActivate: [AppRouteGuard],
          },
          {
            path: "about",
            component: AboutComponent,
            canActivate: [AppRouteGuard],
          },
        ],
      },
    ]),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}

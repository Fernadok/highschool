import { Component, Injector, OnInit } from "@angular/core";
import { finalize } from "rxjs/operators";
import { BsModalService, BsModalRef } from "ngx-bootstrap/modal";
import { appModuleAnimation } from "@shared/animations/routerTransition";
import {
  ContextDto,
  PagedListingComponentBase,
  PagedRequestDto,
} from "shared/paged-listing-component-base";
import {
  IntegranteServiceServiceProxy,
  IntegranteDto,
  IntegranteDtoContextDto,
} from "@shared/service-proxies/service-proxies";
import { IntegranteModalComponentComponent } from "./integrante-modal-component/integrante-modal-component.component";

class PagedLocacionsRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

class PagedIntegrantesRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}

@Component({
  selector: "app-integrantes",
  templateUrl: "./integrantes.component.html",
  animations: [appModuleAnimation()],
})
export class IntegrantesComponent
  extends PagedListingComponentBase<IntegranteDto>
  implements OnInit
{
  alumnos: IntegranteDto[] = [];
  keyword = "";
  isActive: boolean | null;

  filter = {
    Id: 0,
    Search: "",
    Page: 1,
    PageSize: 10,
    OrderColumn: "Apellido",
    Order: "desc",
  };

  constructor(
    injector: Injector,
    private _alumnoService: IntegranteServiceServiceProxy,
    _modalService: BsModalService
  ) {
    super(injector, _modalService);
  }

  clearFilters(): void {
    this.keyword = "";
    this.isActive = undefined;
    this.getDataPage(1);
  }

  create(): void {
    this.showCreateOrEditUserDialog();
  }

  edit(alumno: IntegranteDto): void {
    this.showCreateOrEditUserDialog(alumno.id);
  }
  private showCreateOrEditUserDialog(id?: number): void {
    let configs = {
      class: "modal-lg",
      backdrop: true,
      ignoreBackdropClick: true,
      initialState: {
        id: id,
      },
    };

    this.showCreateOrEditDialog(IntegranteModalComponentComponent, configs);
  }

  protected list(
    request: PagedLocacionsRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;

    this._alumnoService
      .get(
        this.filter.Id,
        this.keyword,
        pageNumber,
        this.filter.Order,
        this.filter.PageSize,
        this.filter.OrderColumn,
        null,
        null
      )
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: ContextDto<IntegranteDto>) => {
        this.alumnos = result.data;
        this.showPagingContextDto(result, pageNumber);
      });
  }

  protected delete(alumno: IntegranteDto): void {
    abp.message.confirm(
      this.l("Quiere borrar: " + alumno.apellido),
      undefined,
      (result: boolean) => {
        if (result) {
          this._alumnoService.delete(alumno.id).subscribe(() => {
            abp.notify.success(this.l("SuccessfullyDeleted"));
            this.refresh();
          });
        }
      }
    );
  }
}

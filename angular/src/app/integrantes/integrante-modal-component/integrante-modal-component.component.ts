import {
  Component,
  Injector,
  OnInit,
  EventEmitter,
  Output,
} from "@angular/core";
import { AppComponentBase } from "@shared/app-component-base";
import {
  AdjuntoServiceServiceProxy,
  IntegranteDto,
  IntegranteServiceServiceProxy,
  ComisioneServiceServiceProxy,
  FileDto,
  FileParameter,
  Int64IdDescription,
} from "@shared/service-proxies/service-proxies";
import {
  forEach as _forEach,
  includes as _includes,
  map as _map,
} from "lodash-es";
import { BsModalRef } from "ngx-bootstrap/modal";
import { finalize } from "rxjs/operators";

class archivos implements FileParameter {
  data: any;
  fileName: string;
}

@Component({
  templateUrl: "./integrante-modal-component.component.html",
})
export class IntegranteModalComponentComponent
  extends AppComponentBase
  implements OnInit
{
  id: number;
  title = "Crear";
  saving = false;
  Integrante = new IntegranteDto();
  ComisionesList: Int64IdDescription[];
  checkedComisionesMap: { [key: number]: boolean } = {};
  defaultComisioneCheckedStatus = false;
  fileLoad: File;
  fileBase64: any;
  archicoActual: FileDto;
  generos = [
    {
      id: "Hombre",
      tipo: "Hombre",
    },
    {
      id: "Mujer",
      tipo: "Mujer",
    },
    {
      id: "No Binario",
      tipo: "No Binario",
    },
  ];
  opcionSeleccionado: string = "0";

  //alumnoFormulario: FormGroup;

  @Output() onSave = new EventEmitter<any>();

  constructor(
    injector: Injector,
    public _IntegranteService: IntegranteServiceServiceProxy,
    public _ComisionesService: ComisioneServiceServiceProxy,
    public _AdjutnosService: AdjuntoServiceServiceProxy,
    public bsModalRef: BsModalRef
  ) {
    super(injector);

    // this.alumnoFormulario = new FormGroup({
    //   genero: new FormControl(null),
    // });
  }

  ngOnInit(): void {
    this.title = !this.id ? "Nuevo" : "Editar Integrante";

    this._ComisionesService
      .comisionesList()
      .subscribe((result: Int64IdDescription[]) => {
        this.ComisionesList = result;
      });

    if (this.id) {
      this._IntegranteService.getById(this.id).subscribe((result: IntegranteDto) => {
        this.Integrante = result;
        this.opcionSeleccionado = result.sexo;
        this.setInitialComisionesStatus();
      });
    }
  }

  capturar() {
    this.Integrante.sexo = this.opcionSeleccionado;
  }

  setInitialComisionesStatus(): void {
    _map(this.ComisionesList, (item) => {
      this.checkedComisionesMap[item.id] = this.isComisioneChecked(item.id);
    });
  }

  isComisioneChecked(id: any): boolean {
    return _includes(this.Integrante.comisioneIds, id);
    // return this.defaultComisioneCheckedStatus;
  }

  onComisioneChange(comisione: Int64IdDescription, $event) {
    this.checkedComisionesMap[comisione.id] = $event.target.checked;
  }

  getCheckedCuesoss(): number[] {
    const comisioneIds: number[] = [];
    _forEach(this.checkedComisionesMap, function (value, key) {
      if (value) {
        comisioneIds.push(parseInt(key));
      }
    });
    return comisioneIds;
  }

  save(): void {
    this.saving = true;

    this.Integrante.comisioneIds = this.getCheckedCuesoss();

    this._IntegranteService
      .addOrUpdate(this.Integrante)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l("SavedSuccessfully"));
        this.bsModalRef.hide();
        this.onSave.emit();
      });
  }

  archivoSeleccionadoEvent(file: File) {
    this.fileLoad = file;
    const foto = new archivos();
    foto.data = file;
    foto.fileName = file.name;

    this._AdjutnosService.upload(foto).subscribe((result: FileDto) => {
      this.archicoActual = result;
    });

    // toBase64(file)
    //   .then((value: string) => {
    //     this.fileBase64 = value.split("base64,")[1];
    //     console.log("Binarios: ", value);
    //   })
    //   .catch((error) => console.error(error));
  }
}

import { ComisionesVTemplatePage } from './app.po';

describe('ComisionesV App', function() {
  let page: ComisionesVTemplatePage;

  beforeEach(() => {
    page = new ComisionesVTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

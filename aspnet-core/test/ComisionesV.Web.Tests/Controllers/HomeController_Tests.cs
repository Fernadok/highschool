﻿using System.Threading.Tasks;
using ComisionesV.Models.TokenAuth;
using ComisionesV.Web.Controllers;
using Shouldly;
using Xunit;

namespace ComisionesV.Web.Tests.Controllers
{
    public class HomeController_Tests: ComisionesVWebTestBase
    {
        [Fact]
        public async Task Index_Test()
        {
            await AuthenticateAsync(null, new AuthenticateModel
            {
                UserNameOrEmailAddress = "admin",
                Password = "123qwe"
            });

            //Act
            var response = await GetResponseAsStringAsync(
                GetUrl<HomeController>(nameof(HomeController.Index))
            );

            //Assert
            response.ShouldNotBeNullOrEmpty();
        }
    }
}
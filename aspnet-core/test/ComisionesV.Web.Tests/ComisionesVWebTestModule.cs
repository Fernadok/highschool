﻿using Abp.AspNetCore;
using Abp.AspNetCore.TestBase;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ComisionesV.EntityFrameworkCore;
using ComisionesV.Web.Startup;
using Microsoft.AspNetCore.Mvc.ApplicationParts;

namespace ComisionesV.Web.Tests
{
    [DependsOn(
        typeof(ComisionesVWebMvcModule),
        typeof(AbpAspNetCoreTestBaseModule)
    )]
    public class ComisionesVWebTestModule : AbpModule
    {
        public ComisionesVWebTestModule(ComisionesVEntityFrameworkModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbContextRegistration = true;
        } 
        
        public override void PreInitialize()
        {
            Configuration.UnitOfWork.IsTransactional = false; //EF Core InMemory DB does not support transactions.
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ComisionesVWebTestModule).GetAssembly());
        }
        
        public override void PostInitialize()
        {
            IocManager.Resolve<ApplicationPartManager>()
                .AddApplicationPartsIfNotAddedBefore(typeof(ComisionesVWebMvcModule).Assembly);
        }
    }
}
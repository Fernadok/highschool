﻿namespace ComisionesV.Authorization
{
    public static class PermissionNames
    {
        public const string Pages_Tenants = "Pages.Tenants";

        public const string Pages_Capacitadores     = "Pages.Capacitadores";
        public const string Pages_Comisiones            = "Pages.Comisiones";
        public const string Pages_Integrantes           = "Pages.Integrantes";
        public const string Pages_Locacioines       = "Pages.Locacioines";
        public const string Pages_Adjuntos          = "Pages.Adjuntos";
        public const string Pages_Inscripcion       = "Pages_Inscripcion";
        public const string Pages_Users             = "Pages.Users";
        public const string Pages_Users_Activation  = "Pages.Users.Activation";

        public const string Pages_Roles = "Pages.Roles";
    }
}

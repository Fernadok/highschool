﻿using Abp.Authorization;
using ComisionesV.Authorization.Roles;
using ComisionesV.Authorization.Users;

namespace ComisionesV.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}

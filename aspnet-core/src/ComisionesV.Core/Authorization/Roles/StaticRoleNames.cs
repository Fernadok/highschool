namespace ComisionesV.Authorization.Roles
{
    public static class StaticRoleNames
    {
        public static class Host
        {
            public const string SuperSU = "SuperSU";
            public const string Administrador = "Administrador CC";
            public const string Operador = "Operador CC";
        }

        public static class Tenants
        {
            public const string Admin = "SuperSU";
        }
    }
}

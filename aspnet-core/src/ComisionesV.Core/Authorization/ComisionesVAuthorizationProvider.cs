﻿using Abp.Authorization;
using Abp.Localization;
using Abp.MultiTenancy;

namespace ComisionesV.Authorization
{
    public class ComisionesVAuthorizationProvider : AuthorizationProvider
    {
        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            context.CreatePermission(PermissionNames.Pages_Capacitadores, L("Capacitadores"));
            context.CreatePermission(PermissionNames.Pages_Comisiones, L("Comisiones"));
            context.CreatePermission(PermissionNames.Pages_Integrantes, L("Integrantes"));
            context.CreatePermission(PermissionNames.Pages_Locacioines, L("Locacioines"));
            context.CreatePermission(PermissionNames.Pages_Adjuntos, L("Adjuntos"));
            //
            context.CreatePermission(PermissionNames.Pages_Inscripcion, L("Inscripcion"));

            context.CreatePermission(PermissionNames.Pages_Users, L("Users"));
            context.CreatePermission(PermissionNames.Pages_Users_Activation, L("UsersActivation"));
            context.CreatePermission(PermissionNames.Pages_Roles, L("Roles"));
            context.CreatePermission(PermissionNames.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, ComisionesVConsts.LocalizationSourceName);
        }
    }
}

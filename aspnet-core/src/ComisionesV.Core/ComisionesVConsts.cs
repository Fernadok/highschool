﻿namespace ComisionesV
{
    public class ComisionesVConsts
    {
        public const string LocalizationSourceName = "ComisionesV";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = false;

        public const string IntegrantesAttachements = "Integrantes";
    }
}

﻿using ComisionesV.Base;
using System;

namespace ComisionesV.Domain.Relations
{
    public class ComisioneIntegrante : EntityBase
    {
        public long ComisioneId { get; set; }
        public virtual Comisione Comisione { get; set; }

        public long IntegranteId { get; set; }
        public virtual Integrante Integrante { get; set; }
       
        
        public string TipoAutoridad { get; set; }
        public Decimal Calificacion { get; set; }
        public string Observacion { get; set; }
    }
}

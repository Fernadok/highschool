﻿using ComisionesV.Base;
using ComisionesV.Domain.Relations;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ComisionesV.Domain
{
    public class Comisione : EntityBase
    {

        public string Nombre { get; set; }
        public DateTime FechaInicio { get; set; }

        #region Relations
        public long LocacionId { get; set; }
        public Locacion Locacion { get; set; }


        [JsonIgnore]
        public virtual HashSet<ComisioneIntegrante> ComisioneIntegranteRelations { get; set; }

        #endregion

    }
}

﻿using ComisionesV.Base;
using ComisionesV.Domain.Relations;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace ComisionesV.Domain
{
    public class Integrante : EntityBase
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Sexo { get; set; }
        public DateTime? FechaNacimiento { get; set; }
        public string Documento { get; set; }
        public string Domicilio { get; set; }
        public string Telefono { get; set; }
        public string Mail { get; set; }
        //
        public string CODIGO_SUBR { get; set; }
        public string MATRICULA { get; set; }
        public string Clase { get; set; }
        public string TIPO_EJEMPLAR { get; set; }
        public string SECC_NUMERO { get; set; }
        public string CIRC_NUMERO { get; set; }
        public string COD_CIRC { get; set; }
        public string NRO_MESA { get; set; }
        public string ORDEN_MESA { get; set; }
        public string ESTABLECIMIENTO { get; set; }
        public string DIRECCION { get; set; }



        #region Relations

        [JsonIgnore]
        public virtual HashSet<ComisioneIntegrante> ComisioneIntegranteRelations { get; set; }
      
        #endregion
    }
}

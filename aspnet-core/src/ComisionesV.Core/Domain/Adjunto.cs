﻿using ComisionesV.Base;

namespace ComisionesV.Domain
{
    public class Adjunto : EntityBase
    {
        public string Ruta { get; set; }
        public long Tamanio { get; set; }
        public string Nombre { get; set; }
    }
}

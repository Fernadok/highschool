﻿using ComisionesV.Base;
using System.Collections.Generic;

namespace ComisionesV.Domain
{
    public class Locacion : EntityBase
    {
        public Locacion()
        {
            Comisiones = new HashSet<Comisione>();
        }

        public string Denominacion { get; set; }

        public virtual HashSet<Comisione> Comisiones { get; set; }
    }
}

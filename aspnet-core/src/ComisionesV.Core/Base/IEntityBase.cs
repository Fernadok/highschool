﻿using Abp.Domain.Entities;

namespace ComisionesV.Base
{
    public interface IEntityBase<TPrimaryKey> : IEntity<TPrimaryKey>
    {
        bool IsActive { get; set; }
    }
}

﻿using Abp.Domain.Entities;
using Abp.Localization;
using System;
using System.Text;

namespace ComisionesV.Base
{
    public abstract class EntityBase<TPrimaryKey> : Entity<TPrimaryKey>, IEntityBase<TPrimaryKey>
    {
        public EntityBase()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }
        public string L(string key)
        {
            return LocalizationHelper.GetString(ComisionesVConsts.LocalizationSourceName, key);
        }
    }

    public abstract class EntityBase : EntityBase<long>
    {
        public EntityBase()
        {
            //Id = NewLong();
        }

        public long NewLong()
        {
            var dateunique = Convert.ToInt64(DateTime.Now.ToString("yymmddhhss"));
            return dateunique;
        }
    }
}

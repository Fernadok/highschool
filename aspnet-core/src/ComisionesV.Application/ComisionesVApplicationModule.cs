﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ComisionesV.Authorization;

namespace ComisionesV
{
    [DependsOn(
        typeof(ComisionesVCoreModule), 
        typeof(AbpAutoMapperModule))]
    public class ComisionesVApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Authorization.Providers.Add<ComisionesVAuthorizationProvider>();
        }

        public override void Initialize()
        {
            var thisAssembly = typeof(ComisionesVApplicationModule).GetAssembly();

            IocManager.RegisterAssemblyByConvention(thisAssembly);

            Configuration.Modules.AbpAutoMapper().Configurators.Add(
                // Scan the assembly for classes which inherit from AutoMapper.Profile
                cfg => cfg.AddMaps(thisAssembly)
            );
        }
    }
}

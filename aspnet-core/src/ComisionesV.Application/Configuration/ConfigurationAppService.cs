﻿using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Runtime.Session;
using ComisionesV.Configuration.Dto;

namespace ComisionesV.Configuration
{
    [AbpAuthorize]
    public class ConfigurationAppService : ComisionesVAppServiceBase, IConfigurationAppService
    {
        public async Task ChangeUiTheme(ChangeUiThemeInput input)
        {
            await SettingManager.ChangeSettingForUserAsync(AbpSession.ToUserIdentifier(), AppSettingNames.UiTheme, input.Theme);
        }
    }
}

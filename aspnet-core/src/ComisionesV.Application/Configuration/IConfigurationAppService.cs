﻿using System.Threading.Tasks;
using ComisionesV.Configuration.Dto;

namespace ComisionesV.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}

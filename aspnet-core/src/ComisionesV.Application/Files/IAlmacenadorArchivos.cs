﻿using ComisionesV.DTOs;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ComisionesV.Files
{
    public interface IAlmacenadorArchivos
    {
        Task BorrarArchivo(string ruta, string contenedor);
        Task<string> EditarArchivo(string contenedor, IFormFile archivoo, string ruta);
        Task<string> GuardarArchivo(string contenedor, IFormFile archivo);

    }
}

﻿using Abp.Application.Services.Dto;

namespace ComisionesV.Roles.Dto
{
    public class PagedRoleResultRequestDto : PagedResultRequestDto
    {
        public string Keyword { get; set; }
    }
}


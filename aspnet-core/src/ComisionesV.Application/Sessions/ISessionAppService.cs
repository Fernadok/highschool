﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ComisionesV.Sessions.Dto;

namespace ComisionesV.Sessions
{
    public interface ISessionAppService : IApplicationService
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformations();
    }
}

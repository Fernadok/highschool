using System.ComponentModel.DataAnnotations;

namespace ComisionesV.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
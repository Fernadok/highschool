using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ComisionesV.Authorization.Users;
using ComisionesV.Roles.Dto;
using ComisionesV.Users.Dto;

namespace ComisionesV.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedUserResultRequestDto, CreateUserDto, UserDto>
    {
        Task DeActivate(EntityDto<long> user);
        Task Activate(EntityDto<long> user);
        Task<ListResultDto<RoleDto>> GetRoles();
        Task ChangeLanguage(ChangeUserLanguageDto input);

        Task<bool> ChangePassword(ChangePasswordDto input);

        Task<User> GetUser(string userName);
    }
}

﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Uow;
using Abp.UI;
using ComisionesV.Authorization;
using ComisionesV.Base;
using ComisionesV.DTOs;
using ComisionesV.Extensions;
using NinjaNye.SearchExtensions;
using PagedList;
using System.Linq;

namespace ComisionesV.ComisioneIntegrantes
{
    //[AbpAuthorize(PermissionNames.Pages_Comisiones, PermissionNames.Pages_Integrantes)]
    public class ComisioneIntegranteService : ComisionesVAppServiceBase, IComisioneIntegranteService
    {
        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(ComisioneIntegranteDto dto)
        {
           
            if (!dto.ComisioneIntegrantes.Any())
            {
                var integrantes = ComisioneIntegranteRepository.GetAll().Where(w => w.ComisioneId == dto.Id).ToList();

                integrantes.ForEach(it => {
                    ComisioneIntegranteRepository.Delete(it);
                });
               // CurrentUnitOfWork.SaveChanges();
            }
            else
            {
                //var integrantes = ComisioneIntegranteRepository.GetAll().Where(w => w.ComisioneId == dto.Id).ToList();
                //var remove = integrantes.Where(s => !dto.ComisioneIntegrantes.Any(x => x.IntegranteId == s.IntegranteId)).ToList();
                //remove.ForEach(it =>
                //{
                //    ComisioneIntegranteRepository.Delete(it);
                //});
                //CurrentUnitOfWork.SaveChanges();
            }

            foreach (var item in dto.ComisioneIntegrantes)
            {
                ComisioneIntegranteRepository.InsertOrUpdate(new Domain.Relations.ComisioneIntegrante
                {
                    Id = item.Id,
                    ComisioneId = item.ComisioneId,
                    IntegranteId = item.IntegranteId,
                    Calificacion = item.Calificacion,
                    TipoAutoridad = item.TipoAutoridad,
                    Observacion = item.Observacion
                });
            }
            CurrentUnitOfWork.SaveChanges();

            return 0;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var cur = ComisioneIntegranteRepository.FirstOrDefault(x => x.Id == id);
            if (cur != null)
            {
                ComisioneIntegranteRepository.Delete(cur);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<ComisioneIntegranteDto> Get(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = ComisioneIntegranteRepository.GetAllIncluding(s => s.Integrante)
                            .Where(x => x.IsActive)
                            .Search(x => x.Integrante.Nombre)
                            .Containing(filters.Search)
                            .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                            .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<ComisioneIntegranteDto>
            {
                Data = resultList.Select(x => x.MapTo<ComisioneIntegranteDto>()),
                MetaData = resultList.GetMetaData()
            };
        }

        public ComisioneIntegranteDto GetById(long id)
        {
            var comisione = ComisioneIntegranteRepository
                        .GetAllIncluding(s => s.Integrante,s => s.Comisione)
                        .Where(x => x.ComisioneId == id && x.IsActive)
                        .ToList();


            var ComisioneIntegranteDto = new ComisioneIntegranteDto();

            foreach (var item in comisione)
            {
                ComisioneIntegranteDto.ComisioneId = item.ComisioneId;
                ComisioneIntegranteDto.Comisione = item.Comisione?.MapTo<ComisioneDto>();
                ComisioneIntegranteDto.ComisioneIntegrantes.Add(new IntegrantesComisioneDto
                {
                    Id = item.Id,
                    ComisioneId = item.ComisioneId,
                    IntegranteId = item.IntegranteId,
                    Calificacion = item.Calificacion,
                    TipoAutoridad=item.TipoAutoridad,
                    Observacion = item.Observacion,
                    NombreCompleto = $"{item.Integrante?.Apellido} {item.Integrante?.Nombre} ({item.Integrante?.Documento})"
                });
            }
            return ComisioneIntegranteDto;
        }
    }
}

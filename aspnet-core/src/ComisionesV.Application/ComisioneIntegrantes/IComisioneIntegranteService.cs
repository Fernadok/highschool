﻿using Abp.Application.Services;
using ComisionesV.Base;
using ComisionesV.DTOs;

namespace ComisionesV.ComisioneIntegrantes
{
    public interface IComisioneIntegranteService : IApplicationService
    {
        ContextDto<ComisioneIntegranteDto> Get(SearchDto filters);

        long AddOrUpdate(ComisioneIntegranteDto dto);

        ComisioneIntegranteDto GetById(long id);

        void Delete(long id);
    }
}
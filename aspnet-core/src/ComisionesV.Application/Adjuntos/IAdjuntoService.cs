﻿using Abp.Application.Services;
using ComisionesV.DTOs;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ComisionesV.Adjuntos
{
    public interface IAdjuntoService : IApplicationService
    {
        Task<FileDto> Upload(IFormFile file);
    }
}

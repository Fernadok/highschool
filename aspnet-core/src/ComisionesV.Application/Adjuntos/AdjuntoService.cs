﻿using Abp.Authorization;
using Abp.AutoMapper;
using ComisionesV.Authorization;
using ComisionesV.DTOs;
using ComisionesV.Files;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ComisionesV.Adjuntos
{
    [AbpAuthorize(PermissionNames.Pages_Adjuntos)]
    public class AdjuntoService : ComisionesVAppServiceBase, IAdjuntoService
    {
        private readonly IAlmacenadorArchivos _almacenadorArchivos;

        public AdjuntoService(IAlmacenadorArchivos almacenadorArchivos)
        {
            _almacenadorArchivos = almacenadorArchivos;
        }

        public async Task<FileDto> Upload(IFormFile file)
        {
            var result = await _almacenadorArchivos.GuardarArchivo(ComisionesVConsts.IntegrantesAttachements, file);

            // Guardar en DB..
            
            return new FileDto
            {
                FileName = file.FileName,
                Size = file.Length,
                Type = file.ContentType,
                Path = result

        };
        }
    }
}

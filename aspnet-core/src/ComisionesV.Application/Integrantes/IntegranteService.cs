﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Uow;
using Abp.UI;
using ComisionesV.Authorization;
using ComisionesV.Authorization.Accounts;
using ComisionesV.Authorization.Accounts.Dto;
using ComisionesV.Base;
using ComisionesV.DapperORM;
using ComisionesV.DTOs;
using ComisionesV.Extensions;
using Microsoft.AspNetCore.Http;
using NinjaNye.SearchExtensions;
using PagedList;
using System.Collections.Generic;
using System.Linq;

namespace ComisionesV.Integrantes
{
    [AbpAuthorize(PermissionNames.Pages_Integrantes)]
    public class IntegranteService : ComisionesVAppServiceBase, IIntegranteService
    {
        //private readonly IAccountAppService _accountAppService;
        private DapperRepository dapperRepository = new();


        public IntegranteService()
        {
            //_accountAppService = accountAppService;

            //var loginMuniModel = new LoginMuniModel
            //{
            //    Sistema = "SISTEMAX",
            //    SistemaPassword = "SISTEMAX",
            //    Usuario = "40369369",
            //    UsuarioPassword = "40369369"
            //};

            //_accountAppService.LoginMuni(loginMuniModel);
        }


        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(IntegranteDto dto, IFormFile archivo)
        {
            var isnew = IntegranteRepository.FirstOrDefault(x => x.Id == dto.Id);

            // -- Verifico si el nombre ya se encuentra registrado
            if (isnew != null && isnew.Documento.ToLower() != dto.Documento.ToLower())
            {
                var existother = IntegranteRepository.FirstOrDefault(x => x.Id != isnew.Id && x.Documento.ToLower() == dto.Documento.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada un documento con el mismo nombre, intente ponerle otro nombre.");
                }
            }
            else if (isnew == null)
            {
                var existother = IntegranteRepository?.FirstOrDefault(x => x.Documento.ToLower() == dto.Documento.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada un documento con el mismo nombre, intente ponerle otro nombre.");
                }
            }

            var integrante = dto.MapTo(isnew);
            IntegranteRepository.InsertOrUpdate(integrante);
            CurrentUnitOfWork.SaveChanges();

            //Add ComisioneRelations
            var curAlum = ComisioneIntegranteRepository.GetAll().Where(w => w.IntegranteId == integrante.Id).ToList();
            foreach (var item in curAlum)
            {
                ComisioneIntegranteRepository.Delete(item);
            }
            CurrentUnitOfWork.SaveChanges();

            foreach (var curId in dto.ComisioneIds)
            {
                ComisioneIntegranteRepository.InsertOrUpdate(new Domain.Relations.ComisioneIntegrante
                {
                    IntegranteId = integrante.Id,
                    ComisioneId = curId,
                    IsActive = true
                });
            }
            CurrentUnitOfWork.SaveChanges();

            return integrante.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var integrante = IntegranteRepository.FirstOrDefault(x => x.Id == id);
            if (integrante != null)
            {
                IntegranteRepository.Delete(integrante);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<IntegranteDto> Get(SearchDto filters)
        {
            var totalRows = dapperRepository.GetTotal();

            var res = dapperRepository.GetAllIntegrantes(
                filters.Search,
                filters.Page,
                filters.PageSize,
                filters.OrderColumn,
                filters.Order);

            return new ContextDto<IntegranteDto>
            {
                Data = res.Select(x => x.MapTo<IntegranteDto>()),
                MetaData = new CustomMetaData
                {
                    _PageSize = filters.PageSize,
                    _PageNumber = filters.Page,
                    _TotalItemCount = totalRows,
                    _PageCount = totalRows / filters.PageSize
                }
            };
        }

        public IntegranteDto GetById(long id)
        {
            var integrante = IntegranteRepository.FirstOrDefault(x => x.Id == id);
            var comisiones = ComisioneIntegranteRepository.GetAll()
                        .Where(w => w.IntegranteId == id)
                        .ToList();

            var map = integrante?.MapTo<IntegranteDto>();

            if (comisiones != null && comisiones.Count > 0)
                map.ComisioneIds = comisiones?.Select(s => s.ComisioneId)?.ToList();

            return map;
        }


        public HashSet<IdDescription<long>> IntegranteList()
        {
            var result = IntegranteRepository.GetAll().Where(w => w.IsActive).ToList();

            return result.Select(x => new IdDescription<long>
            {
                Id = x.Id,
                Description = $"{x.Apellido} {x.Nombre} ({x.Documento})"
            })
              .OrderBy(x => x.Description)
              .ToHashSet();
        }

        public HashSet<IdDescription<long>> IntegranteList2(string filter)
        {
            var result = dapperRepository.GetAllIntegrantes(filter, 1, 40, "Nombre", "desc");
        
            return result.Select(x => new IdDescription<long>
            {
                Id = x.Id,
                Description = $"{x.Apellido} {x.Nombre} ({x.Documento})"
            })
           .OrderBy(x => x.Description)
           .ToHashSet();
        }


    }
}

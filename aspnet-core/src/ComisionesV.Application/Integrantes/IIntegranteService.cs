﻿using Abp.Application.Services;
using ComisionesV.Base;
using ComisionesV.DTOs;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.Integrantes
{
    public interface IIntegranteService : IApplicationService
    {
        ContextDto<IntegranteDto> Get(SearchDto filters);

        long AddOrUpdate(IntegranteDto dto, IFormFile archivo);

        IntegranteDto GetById(long id);

        void Delete(long id);

        HashSet<IdDescription<long>> IntegranteList();

        HashSet<IdDescription<long>> IntegranteList2(string filter);

    }
}

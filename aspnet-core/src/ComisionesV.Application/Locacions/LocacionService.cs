﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Uow;
using Abp.UI;
using ComisionesV.Authorization;
using ComisionesV.Base;
using ComisionesV.DTOs;
using ComisionesV.Extensions;
using NinjaNye.SearchExtensions;
using PagedList;
using System.Collections.Generic;
using System.Linq;

namespace ComisionesV.Locacions
{
    [AbpAuthorize(PermissionNames.Pages_Locacioines)]
    public class LocacionService : ComisionesVAppServiceBase, ILocacionService
    {
        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(LocacionDto dto)
        {
            var isnew = LocacionRepository.FirstOrDefault(x => x.Id == dto.Id);

            // -- Verifico si el nombre ya se encuentra registrado
            if (isnew != null && isnew.Denominacion.ToLower() != dto.Denominacion.ToLower())
            {
                var existother = LocacionRepository.FirstOrDefault(x => x.Id != isnew.Id && x.Denominacion.ToLower() == dto.Denominacion.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada una denominacion con el mismo nombre, intente ponerle otro nombre.");
                }
            }
            else if (isnew == null)
            {
                var existother = LocacionRepository.FirstOrDefault(x => x.Denominacion.ToLower() == dto.Denominacion.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada una denominacion con el mismo nombre, intente ponerle otro nombre.");
                }
            }

            var art = dto.MapTo(isnew);
            LocacionRepository.InsertOrUpdate(art);
            CurrentUnitOfWork.SaveChanges();

            return art.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var loc = LocacionRepository.FirstOrDefault(x => x.Id == id);
            if (loc != null)
            {
                LocacionRepository.Delete(loc);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<LocacionDto> Get(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = LocacionRepository.GetAll()
                            .Where(x => x.IsActive)
                            .Search(x => x.Denominacion)
                            .Containing(filters.Search)
                            .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                            .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<LocacionDto>
            {
                Data = resultList.Select(x => x.MapTo<LocacionDto>()),
                MetaData = resultList.GetMetaData()
            };
        }

        public LocacionDto GetById(long id)
        {
            var loc = LocacionRepository.FirstOrDefault(x => x.Id == id);
            return loc?.MapTo<LocacionDto>();
        }

        public HashSet<IdDescription<long>> LocacionesList()
        {
            var result = LocacionRepository.GetAll()
                 .Where(x => x.IsActive).ToList();

            return result.Select(x => new IdDescription<long>
            {
                Id = x.Id,
                Description = x.Denominacion
            })
              .OrderBy(x => x.Description)
              .ToHashSet();
        }
    }
}

﻿using Abp.Application.Services;
using ComisionesV.Base;
using ComisionesV.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.Locacions
{
    public interface ILocacionService : IApplicationService
    {
        ContextDto<LocacionDto> Get(SearchDto filters);

        long AddOrUpdate(LocacionDto dto);

        LocacionDto GetById(long id);

        void Delete(long id);

        HashSet<IdDescription<long>> LocacionesList();
    }
}

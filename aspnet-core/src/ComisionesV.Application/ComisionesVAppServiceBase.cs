﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Abp.Application.Services;
using Abp.IdentityFramework;
using Abp.Runtime.Session;
using ComisionesV.Authorization.Users;
using ComisionesV.MultiTenancy;
using Abp.Domain.Repositories;
using ComisionesV.Domain;
using ComisionesV.Domain.Relations;
using ComisionesV.EntityFrameworkCore;
using ComisionesV.Authorization.Roles;
using System.Linq;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization.Users;

namespace ComisionesV
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class ComisionesVAppServiceBase : ApplicationService
    {
        public IComisionesVDbContext ComisionesVPCDbContext { get; set; }

        public IRepository<Locacion, long> LocacionRepository { get; set; }
        public IRepository<Comisione, long> ComisioneRepository { get; set; }
        public IRepository<Integrante, long> IntegranteRepository { get; set; }
        public IRepository<ComisioneIntegrante, long> ComisioneIntegranteRepository { get; set; }
        public IRepository<Adjunto, long> AdjuntoRepository { get; set; }

        public IRepository<User, long> UserRepository { get; set; }

        public TenantManager TenantManager { get; set; }
        public UserManager UserManager { get; set; }
        public RoleManager RoleManager { get; set; }

        protected ComisionesVAppServiceBase()
        {
            LocalizationSourceName = ComisionesVConsts.LocalizationSourceName;
        }

        protected virtual async Task<User> GetCurrentUserAsync()
        {
            var user = await UserManager.FindByIdAsync(AbpSession.GetUserId().ToString());
            if (user == null)
            {
                throw new Exception("There is no current user!");
            }

            return user;
        }

        protected virtual async Task<string> GetCurrentRolAsync(User user)
        {
            var result = await UserRepository
                        .GetAllIncluding(x => x.Roles)
                        .FirstOrDefaultAsync(x => x.Id == AbpSession.GetUserId());

            var role = await RoleManager.GetRoleByIdAsync(result.Roles.FirstOrDefault().RoleId);

            return role?.NormalizedName;
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}

﻿using Abp.Application.Services;
using ComisionesV.Base;
using ComisionesV.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.Comisiones
{
    public interface IComisioneService : IApplicationService
    {
        ContextDto<ComisioneDto> Get(SearchDto filters);

        long AddOrUpdate(ComisioneDto dto);

        ComisioneDto GetById(long id);

        void Delete(long id);

        HashSet<IdDescription<long>> ComisionesList();
    }
}

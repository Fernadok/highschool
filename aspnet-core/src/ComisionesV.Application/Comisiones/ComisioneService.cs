﻿using Abp.Authorization;
using Abp.AutoMapper;
using Abp.Domain.Uow;
using Abp.UI;
using ComisionesV.Authorization;
using ComisionesV.Base;
using ComisionesV.DTOs;
using ComisionesV.Extensions;
using NinjaNye.SearchExtensions;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.Comisiones
{
    [AbpAuthorize(PermissionNames.Pages_Comisiones)]
    public class ComisioneService : ComisionesVAppServiceBase, IComisioneService
    {
        [UnitOfWork(isTransactional: true)]
        public long AddOrUpdate(ComisioneDto dto)
        {
            var isnew = ComisioneRepository.FirstOrDefault(x => x.Id == dto.Id);

            // -- Verifico si el nombre ya se encuentra registrado
            if (isnew != null && isnew.Nombre.ToLower() != dto.Nombre.ToLower())
            {
                var existother = ComisioneRepository.FirstOrDefault(x => x.Id != isnew.Id && x.Nombre.ToLower() == dto.Nombre.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada un nombre con el mismo nombre, intente ponerle otro nombre.");
                }
            }
            else if (isnew == null)
            {
                var existother = ComisioneRepository.FirstOrDefault(x => x.Nombre.ToLower() == dto.Nombre.ToLower());
                if (existother != null)
                {
                    throw new UserFriendlyException("Ya se encuentra registrada un nombre con el mismo nombre, intente ponerle otro nombre.");
                }
            }

            var comisione = dto.MapTo(isnew);
            ComisioneRepository.InsertOrUpdate(comisione);
            CurrentUnitOfWork.SaveChanges();

            return comisione.Id;
        }

        [UnitOfWork(isTransactional: true)]
        public void Delete(long id)
        {
            var comisione = ComisioneRepository.FirstOrDefault(x => x.Id == id);
            if (comisione != null)
            {
                ComisioneRepository.Delete(comisione);
                CurrentUnitOfWork.SaveChanges();
            }
        }

        public ContextDto<ComisioneDto> Get(SearchDto filters)
        {
            filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
            var resultList = ComisioneRepository.GetAllIncluding( s => s.Locacion)
                            .Where(x => x.IsActive)
                            .Search(x => x.Nombre)
                            .Containing(filters.Search)
                            .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
                            .ToPagedList(filters.Page, filters.PageSize);

            return new ContextDto<ComisioneDto>
            {
                Data = resultList.Select(x => x.MapTo<ComisioneDto>()),
                MetaData = resultList.GetMetaData()
            };
        }

        public ComisioneDto GetById(long id)
        {
            var comisione = ComisioneRepository.FirstOrDefault(x => x.Id == id);
            return comisione?.MapTo<ComisioneDto>();
        }

        public HashSet<IdDescription<long>> ComisionesList()
        {
            var result = ComisioneRepository.GetAllIncluding(s => s.Locacion)
              .Where(x => x.IsActive).ToList();

            return result.Select(x => new IdDescription<long>
            {
                Id = x.Id,
                Description = $"{x.Nombre} - { x.Locacion.Denominacion }"
            })
              .OrderBy(x => x.Description)
              .ToHashSet();
        }

        // Comisiones Disponibles
        //public ContextDto<ComisioneDto> GetDisponibles(SearchDto filters)
        //{
        //    filters.Search = string.IsNullOrEmpty(filters.Search) ? "" : filters.Search.ToLower();
        //    var resultList = ComisioneRepository.GetAllIncluding(s => s.Locacion, s => s.ComisioneIntegranteRelations)
        //                    .Where(x => x.FechaFin >= DateTime.Now && x.Cupo > 0)
        //                    .Search(x => x.Nombre)
        //                    .Containing(filters.Search)
        //                    .OrderByTo(filters.OrderColumn, filters.Order.ToLower() == "desc")
        //                    .ToPagedList(filters.Page, filters.PageSize);

        //    return new ContextDto<ComisioneDto>
        //    {
        //        Data = resultList.Select(x => x.MapTo<ComisioneDto>()),
        //        MetaData = resultList.GetMetaData()
        //    };
        //}

        // Cupos Disponibles 

    }
}

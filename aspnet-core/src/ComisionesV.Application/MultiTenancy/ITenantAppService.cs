﻿using Abp.Application.Services;
using ComisionesV.MultiTenancy.Dto;

namespace ComisionesV.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedTenantResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}


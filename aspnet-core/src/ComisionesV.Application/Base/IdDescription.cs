﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.Base
{
    public class IdDescription<TPrimaryKey>
    {
        public TPrimaryKey Id { get; set; }
        public string Description { get; set; }
        public string DescriptionFull { get; set; }
        public string Created { get; set; }
    }
}

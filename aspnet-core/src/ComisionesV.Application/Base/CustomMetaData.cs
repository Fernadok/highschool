﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.Base
{
    public class CustomMetaData : IPagedList
    {
        public int _PageCount { get; set; }     
        public int PageCount => _PageCount;


        public int _TotalItemCount { get; set; }
        public int TotalItemCount => _TotalItemCount;
        public int _PageNumber { get; set; }
        public int PageNumber => _PageNumber;
        public int _PageSize { get; set; }
        public int PageSize => _PageSize;
        public bool _HasPreviousPage { get; set; }
        public bool HasPreviousPage => _HasPreviousPage;
        public bool _HasNextPage { get; set; }
        public bool HasNextPage => _HasNextPage;
        public bool _IsFirstPage { get; set; }
        public bool IsFirstPage => _IsFirstPage;
        public bool _IsLastPage { get; set; }
        public bool IsLastPage => _IsLastPage;
        public int _FirstItemOnPage { get; set; }
        public int FirstItemOnPage => _FirstItemOnPage;
        public int _LastItemOnPage { get; set; }
        public int LastItemOnPage => _LastItemOnPage;
    }
}

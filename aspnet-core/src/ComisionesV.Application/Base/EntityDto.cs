﻿using Abp.Localization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.Base
{
    [JsonObject(IsReference = false)]
    public class EntityDto<TPrimaryKey> : Abp.Application.Services.Dto.EntityDto<TPrimaryKey>
    {
        public EntityDto()
        {
            IsActive = true;
        }

        public bool IsActive { get; set; }

        public string L(string key)
        {
            return LocalizationHelper.GetString(ComisionesVConsts.LocalizationSourceName, key);
        }
    }

    public class EntityDto : EntityDto<long>
    {

    }
}

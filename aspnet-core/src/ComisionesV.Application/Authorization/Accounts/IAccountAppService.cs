﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ComisionesV.Authorization.Accounts.Dto;

namespace ComisionesV.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);

    
    }
}

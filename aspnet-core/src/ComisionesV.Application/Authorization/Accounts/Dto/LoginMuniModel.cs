﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.Authorization.Accounts.Dto
{
    public class LoginMuniModel
    {
        public string Sistema { get; set; }
        public string SistemaPassword { get; set; }
        public string Usuario { get; set; }
        public string UsuarioPassword { get; set; }
        public string Rol { get; set; }
    }
}

﻿using ComisionesV.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.DTOs
{
    public class FileDto
    {
        public string FileName { get; set; }
        public long Size { get; set; }
        public string Type { get; set; }
        public string Path { get; set; }
 

        public string Extension => Path.GetExtension();

        public string MimeType => Path.GetMimeType();

        public bool IsFileOfDomain => !Path.StartsWith("http");

        public bool IsImage => FileName.IsImage();

        public bool IsVideo => FileName.IsVideo();

        public string SizeTranslated
        {
            get
            {
                string[] sizes = { "B", "KB", "MB", "GB" };
                double len = Size;
                int order = 0;
                while (len >= 1024 && order + 1 < sizes.Length)
                {
                    order++;
                    len /= 1024;
                }
                return string.Format("{0:0.##} {1}", len, sizes[order]);
            }
        }

    }
}

﻿using Abp.AutoMapper;
using ComisionesV.Base;
using ComisionesV.Domain;
using ComisionesV.Domain.Relations;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.DTOs
{
    [AutoMap(typeof(Comisione))]
    public class ComisioneDto : EntityDto
    {
        public string Nombre { get; set; }
        public DateTime FechaInicio { get; set; }
        public long LocacionId { get; set; }
        public LocacionDto Locacion { get; set; }

        //public int Inscriptos => ComisioneIntegranteRelations== null ? 0 : ComisioneIntegranteRelations.Select(x => x.IntegranteId).Count();
        //public int CuposVacantes => Cupo - Inscriptos;

        [JsonIgnore]
        public virtual HashSet<ComisioneIntegrante> ComisioneIntegranteRelations { get; set; }

    }
}

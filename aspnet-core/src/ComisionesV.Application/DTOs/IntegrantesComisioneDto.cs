﻿using ComisionesV.Base;
using ComisionesV.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.DTOs
{
    public class IntegrantesComisioneDto : EntityBase
    {
        public long ComisioneId { get; set; }
        public virtual Comisione Comisione { get; set; }

        public long IntegranteId { get; set; }
        public virtual Integrante Integrante { get; set; }


        public string TipoAutoridad { get; set; }
        public Decimal Calificacion { get; set; }
        public string Observacion { get; set; }

        public string NombreCompleto { get; set; }
    }
}

﻿using Abp.AutoMapper;
using ComisionesV.Base;
using ComisionesV.Domain.Relations;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace ComisionesV.DTOs
{
    [AutoMap(typeof(ComisioneIntegrante))]
    public class ComisioneIntegranteDto : EntityDto
    {
        public ComisioneIntegranteDto()
        {
            ComisioneIntegrantes = new List<IntegrantesComisioneDto>();
        }

        public long ComisioneId { get; set; }
        
        public virtual ComisioneDto Comisione { get; set; }


        public List<IntegrantesComisioneDto> ComisioneIntegrantes { get; set; }
 
    }
}

﻿using Abp.AutoMapper;
using ComisionesV.Base;
using ComisionesV.Domain;

namespace ComisionesV.DTOs
{
    [AutoMap(typeof(Locacion))]
    public class LocacionDto : EntityDto
    {
        public string Denominacion { get; set; }
    }
}

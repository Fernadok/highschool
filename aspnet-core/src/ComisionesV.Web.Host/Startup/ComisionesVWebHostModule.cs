﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ComisionesV.Configuration;

namespace ComisionesV.Web.Host.Startup
{
    [DependsOn(
       typeof(ComisionesVWebCoreModule))]
    public class ComisionesVWebHostModule: AbpModule
    {
        private readonly IWebHostEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public ComisionesVWebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ComisionesVWebHostModule).GetAssembly());
        }
    }
}

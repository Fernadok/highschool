﻿using ComisionesV.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ComisionesV.EntityFrameworkCore.Configurations
{
    public class LocacionConfiguration : IEntityTypeConfiguration<Locacion>
    {
        public void Configure(EntityTypeBuilder<Locacion> builder)
        {
            builder.HasKey(x => x.Id);
        }
    }
}

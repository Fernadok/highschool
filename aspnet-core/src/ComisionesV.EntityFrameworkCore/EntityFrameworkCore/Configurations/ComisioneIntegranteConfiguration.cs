﻿using ComisionesV.Domain.Relations;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ComisionesV.EntityFrameworkCore.Configurations
{
    public class ComisioneIntegranteConfiguration : IEntityTypeConfiguration<ComisioneIntegrante>
    {
        public void Configure(EntityTypeBuilder<ComisioneIntegrante> builder)
        {
            builder.HasKey(x => x.Id);

            builder
               .HasOne(p => p.Integrante)
               .WithMany(c => c.ComisioneIntegranteRelations)
               .HasForeignKey(p => p.IntegranteId)
               .IsRequired();

            builder
                .HasOne(p => p.Comisione)
                .WithMany(c => c.ComisioneIntegranteRelations)
                .HasForeignKey(p => p.ComisioneId)
                .IsRequired();
        }
    }
}

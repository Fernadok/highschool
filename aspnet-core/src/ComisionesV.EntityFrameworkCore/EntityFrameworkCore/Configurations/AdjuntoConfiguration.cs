﻿using ComisionesV.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ComisionesV.EntityFrameworkCore.Configurations
{
    public class AdjuntoConfiguration : IEntityTypeConfiguration<Adjunto>
    {
        public void Configure(EntityTypeBuilder<Adjunto> builder)
        {
            builder.HasKey(x => x.Id);
        }
    }
}

﻿using ComisionesV.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace ComisionesV.EntityFrameworkCore.Configurations
{
    public class ComisioneConfiguration : IEntityTypeConfiguration<Comisione>
    {
        public void Configure(EntityTypeBuilder<Comisione> builder)
        {
            builder.HasKey(x => x.Id);

            builder
               .HasOne(p => p.Locacion)
               .WithMany(c => c.Comisiones)
               .HasForeignKey(p => p.LocacionId)
               .IsRequired();

        }
    }
}

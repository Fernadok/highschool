using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace ComisionesV.EntityFrameworkCore
{
    public static class ComisionesVDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<ComisionesVDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<ComisionesVDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}

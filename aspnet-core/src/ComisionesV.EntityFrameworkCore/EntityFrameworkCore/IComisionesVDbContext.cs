﻿using ComisionesV.Domain;
using ComisionesV.Domain.Relations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.EntityFrameworkCore
{
    public interface IComisionesVDbContext
    {
        DbSet<Adjunto> Adjuntos { get; set; }
        DbSet<Integrante> Integrantes { get; set; }
        DbSet<Comisione> Comisiones { get; set; }
        DbSet<Locacion> Locacions { get; set; }
        DbSet<ComisioneIntegrante> ComisioneIntegranteRelations { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using ComisionesV.Configuration;
using ComisionesV.Web;

namespace ComisionesV.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class ComisionesVDbContextFactory : IDesignTimeDbContextFactory<ComisionesVDbContext>
    {
        public ComisionesVDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ComisionesVDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            ComisionesVDbContextConfigurer.Configure(builder, configuration.GetConnectionString(ComisionesVConsts.ConnectionStringName));

            return new ComisionesVDbContext(builder.Options);
        }
    }
}

using System.Linq;
using Microsoft.EntityFrameworkCore;
using Abp.Authorization;
using Abp.Authorization.Roles;
using Abp.Authorization.Users;
using Abp.MultiTenancy;
using ComisionesV.Authorization;
using ComisionesV.Authorization.Roles;
using ComisionesV.Authorization.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;

namespace ComisionesV.EntityFrameworkCore.Seed.Host
{
    public class HostRoleAndUserCreator
    {
        private readonly ComisionesVDbContext _context;

        public HostRoleAndUserCreator(ComisionesVDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateHostRoleAndUsers();
        }

        private void CreateHostRoleAndUsers()
        {
            // Admin role for host

            var adminRoleForHost = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == null && r.Name == StaticRoleNames.Host.SuperSU);
            if (adminRoleForHost == null)
            {
                adminRoleForHost = _context.Roles.Add(new Role(null, StaticRoleNames.Host.SuperSU, StaticRoleNames.Host.SuperSU) { IsStatic = true, IsDefault = true }).Entity;
                _context.SaveChanges();
            }

            var admininistradorForHost = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == 1 && r.Name == StaticRoleNames.Host.Administrador);
            if (admininistradorForHost == null)
            {
                adminRoleForHost = _context.Roles.Add(new Role(1, StaticRoleNames.Host.Administrador, StaticRoleNames.Host.Administrador) { IsStatic = true, IsDefault = true }).Entity;
                _context.SaveChanges();
            }

            var operadorRoleForHost = _context.Roles.IgnoreQueryFilters().FirstOrDefault(r => r.TenantId == 1 && r.Name == StaticRoleNames.Host.Operador);
            if (operadorRoleForHost == null)
            {
                adminRoleForHost = _context.Roles.Add(new Role(1, StaticRoleNames.Host.Operador, StaticRoleNames.Host.Operador) { IsStatic = true, IsDefault = true }).Entity;
                _context.SaveChanges();
            }

            // Grant all permissions to admin role for host

            var grantedPermissions = _context.Permissions.IgnoreQueryFilters()
                .OfType<RolePermissionSetting>()
                .Where(p => p.TenantId == null && p.RoleId == adminRoleForHost.Id)
                .Select(p => p.Name)
                .ToList();

            var permissions = PermissionFinder
                .GetAllPermissions(new ComisionesVAuthorizationProvider())
                .Where(p => p.MultiTenancySides.HasFlag(MultiTenancySides.Host) &&
                            !grantedPermissions.Contains(p.Name))
                .ToList();

            if (permissions.Any())
            {
                _context.Permissions.AddRange(
                    permissions.Select(permission => new RolePermissionSetting
                    {
                        TenantId = null,
                        Name = permission.Name,
                        IsGranted = true,
                        RoleId = adminRoleForHost.Id
                    })
                );
                _context.SaveChanges();
            }

            // Admin user for host

            var adminUserForHost = _context.Users.IgnoreQueryFilters().FirstOrDefault(u => u.TenantId == null && u.UserName == AbpUserBase.AdminUserName);
            if (adminUserForHost == null)
            {
                var user = new User
                {
                    TenantId = null,
                    UserName = "SUPERSU",
                    Name = "SUPERSU",
                    Surname = "SUPERSU",
                    EmailAddress = "admin@muniAdmin.com",
                    IsEmailConfirmed = true,
                    IsActive = true
                };

                user.Password = new PasswordHasher<User>(new OptionsWrapper<PasswordHasherOptions>(new PasswordHasherOptions())).HashPassword(user, "muni123qwe");
                user.SetNormalizedNames();

                adminUserForHost = _context.Users.Add(user).Entity;
                _context.SaveChanges();

                // Assign Admin role to admin user
                _context.UserRoles.Add(new UserRole(null, adminUserForHost.Id, adminRoleForHost.Id));
                _context.SaveChanges();

                _context.SaveChanges();
            }
        }
    }
}

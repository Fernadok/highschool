﻿using Microsoft.EntityFrameworkCore;
using Abp.Zero.EntityFrameworkCore;
using ComisionesV.Authorization.Roles;
using ComisionesV.Authorization.Users;
using ComisionesV.MultiTenancy;
using ComisionesV.Domain;
using ComisionesV.Domain.Relations;
using ComisionesV.EntityFrameworkCore.Configurations;

namespace ComisionesV.EntityFrameworkCore
{
    public class ComisionesVDbContext : AbpZeroDbContext<Tenant, Role, User, ComisionesVDbContext>, IComisionesVDbContext
    {
        /* Define a DbSet for each entity of the application */
        public virtual DbSet<Adjunto> Adjuntos { get; set; }
        public virtual DbSet<Locacion> Locacions { get; set; }
        public virtual DbSet<Comisione> Comisiones { get; set; }
        public virtual DbSet<Integrante> Integrantes { get; set; }
        public virtual DbSet<ComisioneIntegrante> ComisioneIntegranteRelations { get; set; }

        public ComisionesVDbContext(DbContextOptions<ComisionesVDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AdjuntoConfiguration());
            modelBuilder.ApplyConfiguration(new LocacionConfiguration());
            modelBuilder.ApplyConfiguration(new ComisioneIntegranteConfiguration());
            modelBuilder.ApplyConfiguration(new IntegranteConfiguration());
            modelBuilder.ApplyConfiguration(new ComisioneIntegranteConfiguration());

            base.OnModelCreating(modelBuilder);
        }

    }
}

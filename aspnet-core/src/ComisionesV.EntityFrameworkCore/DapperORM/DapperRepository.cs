﻿using ComisionesV.Configuration;
using ComisionesV.Domain;
using ComisionesV.Web;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComisionesV.DapperORM
{
    public class DapperRepository
    {
        private string connectionString = string.Empty;

        public DapperRepository()
        {
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());
            connectionString = configuration.GetConnectionString(ComisionesVConsts.ConnectionStringName);
        }

        public int GetTotal()
        {
            int coubtRows = 0;

            using (SqlConnection connection = new(connectionString))
            {
                var query = "SELECT COUNT(id) FROM[ComisionesVDb2].[dbo].[Integrantes]";

                coubtRows = connection.QueryFirst<int>(query);
            }

            return coubtRows;
        }

        public List<Integrante> GetAllIntegrantes(string search, int page, int pageSize, string column, string order)
        {
            int offset = page == 1 ? 0 : ((page * pageSize) - pageSize);

            using (SqlConnection connection = new(connectionString))
            {
                var eventName = connection.Query<Integrante>(@$"SELECT [Id]
                                                                          ,[Nombre]
                                                                          ,[Apellido]
                                                                          ,[Sexo]
                                                                          ,[FechaNacimiento]
                                                                          ,[IsActive]
                                                                          ,[Documento]
                                                                          ,[Domicilio]
                                                                          ,[Mail]
                                                                          ,[Telefono]
                                                                          ,[CIRC_NUMERO]
                                                                          ,[CODIGO_SUBR]
                                                                          ,[COD_CIRC]
                                                                          ,[Clase]
                                                                          ,[DIRECCION]
                                                                          ,[ESTABLECIMIENTO]
                                                                          ,[MATRICULA]
                                                                          ,[NRO_MESA]
                                                                          ,[ORDEN_MESA]
                                                                          ,[SECC_NUMERO]
                                                                          ,[TIPO_EJEMPLAR]
                                                                          FROM[ComisionesVDb2].[dbo].[Integrantes]
                                                                          WHERE [Documento] like '%{search}%' or [Nombre] like '%{search}%'
                                                                          order by [{column}] {order}
                                                                          OFFSET {offset} ROWS FETCH NEXT {pageSize} ROWS ONLY");

                return eventName?.ToList();
            }
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ComisionesV.Migrations
{
    public partial class agregocamposenintegrante : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "CIRC_NUMERO",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "CODIGO_SUBR",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "COD_CIRC",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Clase",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DIRECCION",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ESTABLECIMIENTO",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "MATRICULA",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NRO_MESA",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ORDEN_MESA",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SECC_NUMERO",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TIPO_EJEMPLAR",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CIRC_NUMERO",
                table: "Integrantes");

            migrationBuilder.DropColumn(
                name: "CODIGO_SUBR",
                table: "Integrantes");

            migrationBuilder.DropColumn(
                name: "COD_CIRC",
                table: "Integrantes");

            migrationBuilder.DropColumn(
                name: "Clase",
                table: "Integrantes");

            migrationBuilder.DropColumn(
                name: "DIRECCION",
                table: "Integrantes");

            migrationBuilder.DropColumn(
                name: "ESTABLECIMIENTO",
                table: "Integrantes");

            migrationBuilder.DropColumn(
                name: "MATRICULA",
                table: "Integrantes");

            migrationBuilder.DropColumn(
                name: "NRO_MESA",
                table: "Integrantes");

            migrationBuilder.DropColumn(
                name: "ORDEN_MESA",
                table: "Integrantes");

            migrationBuilder.DropColumn(
                name: "SECC_NUMERO",
                table: "Integrantes");

            migrationBuilder.DropColumn(
                name: "TIPO_EJEMPLAR",
                table: "Integrantes");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ComisionesV.Migrations
{
    public partial class ChangeObservacion : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Observación",
                table: "ComisioneIntegranteRelations",
                newName: "Observacion");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Observacion",
                table: "ComisioneIntegranteRelations",
                newName: "Observación");
        }
    }
}

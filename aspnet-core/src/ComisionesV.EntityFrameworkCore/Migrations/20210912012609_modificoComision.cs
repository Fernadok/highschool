﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ComisionesV.Migrations
{
    public partial class modificoComision : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cupo",
                table: "Comisiones");

            migrationBuilder.DropColumn(
                name: "FechaFin",
                table: "Comisiones");

            migrationBuilder.DropColumn(
                name: "Horario",
                table: "Comisiones");

            migrationBuilder.DropColumn(
                name: "Horas",
                table: "Comisiones");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Cupo",
                table: "Comisiones",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "FechaFin",
                table: "Comisiones",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Horario",
                table: "Comisiones",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Horas",
                table: "Comisiones",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}

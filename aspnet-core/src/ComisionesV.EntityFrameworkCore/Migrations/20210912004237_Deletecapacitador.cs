﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ComisionesV.Migrations
{
    public partial class Deletecapacitador : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Comisiones_Capacitadors_CapacitadorId",
                table: "Comisiones");

            migrationBuilder.DropTable(
                name: "Capacitadors");

            migrationBuilder.DropIndex(
                name: "IX_Comisiones_CapacitadorId",
                table: "Comisiones");

            migrationBuilder.DropColumn(
                name: "CapacitadorId",
                table: "Comisiones");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<long>(
                name: "CapacitadorId",
                table: "Comisiones",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.CreateTable(
                name: "Capacitadors",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Apellido = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Documento = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Nombre = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Capacitadors", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Comisiones_CapacitadorId",
                table: "Comisiones",
                column: "CapacitadorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Comisiones_Capacitadors_CapacitadorId",
                table: "Comisiones",
                column: "CapacitadorId",
                principalTable: "Capacitadors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ComisionesV.Migrations
{
    public partial class UpdateDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Cupo",
                table: "Comisiones",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "FechaFin",
                table: "Comisiones",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "FechaInicio",
                table: "Comisiones",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "Horario",
                table: "Comisiones",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "Cumplido",
                table: "ComisioneIntegranteRelations",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Observación",
                table: "ComisioneIntegranteRelations",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Domicilio",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Mail",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Telefono",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cupo",
                table: "Comisiones");

            migrationBuilder.DropColumn(
                name: "FechaFin",
                table: "Comisiones");

            migrationBuilder.DropColumn(
                name: "FechaInicio",
                table: "Comisiones");

            migrationBuilder.DropColumn(
                name: "Horario",
                table: "Comisiones");

            migrationBuilder.DropColumn(
                name: "Cumplido",
                table: "ComisioneIntegranteRelations");

            migrationBuilder.DropColumn(
                name: "Observación",
                table: "ComisioneIntegranteRelations");

            migrationBuilder.DropColumn(
                name: "Domicilio",
                table: "Integrantes");

            migrationBuilder.DropColumn(
                name: "Mail",
                table: "Integrantes");

            migrationBuilder.DropColumn(
                name: "Telefono",
                table: "Integrantes");
        }
    }
}

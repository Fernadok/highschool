﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ComisionesV.Migrations
{
    public partial class Alter1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Documento",
                table: "Capacitadors",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Documento",
                table: "Integrantes",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Documento",
                table: "Capacitadors");

            migrationBuilder.DropColumn(
                name: "Documento",
                table: "Integrantes");
        }
    }
}

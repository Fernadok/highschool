﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ComisionesV.Migrations
{
    public partial class agregoTipoAutoridad : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Cumplido",
                table: "ComisioneIntegranteRelations");

            migrationBuilder.AddColumn<string>(
                name: "TipoAutoridad",
                table: "ComisioneIntegranteRelations",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TipoAutoridad",
                table: "ComisioneIntegranteRelations");

            migrationBuilder.AddColumn<bool>(
                name: "Cumplido",
                table: "ComisioneIntegranteRelations",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}

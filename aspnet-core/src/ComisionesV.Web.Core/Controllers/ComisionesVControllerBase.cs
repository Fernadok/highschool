using Abp.AspNetCore.Mvc.Controllers;
using Abp.IdentityFramework;
using Microsoft.AspNetCore.Identity;

namespace ComisionesV.Controllers
{
    public abstract class ComisionesVControllerBase: AbpController
    {
        protected ComisionesVControllerBase()
        {
            LocalizationSourceName = ComisionesVConsts.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}

﻿using ComisionesV.Authorization.Accounts.Dto;
using System.Threading.Tasks;

namespace ComisionesV.Authentication.External
{
    public interface IExternalAuthManager
    {
        Task<bool> IsValidUser(string provider, string providerKey, string providerAccessCode);

        Task<ExternalAuthUserInfo> GetUserInfo(string provider, string accessCode);

    }
}

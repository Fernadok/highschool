﻿using System;
using Microsoft.IdentityModel.Tokens;

namespace ComisionesV.Authentication.JwtBearer
{
    public class TokenAuthConfiguration
    {
        public SymmetricSecurityKey SecurityKey { get; set; }

        public string Issuer { get; set; }

        public string Audience { get; set; }

        public SigningCredentials SigningCredentials { get; set; }

        public TimeSpan Expiration { get; set; }

        public string Login { get; set; }
        public string Roles { get; set; }   
    }
}
